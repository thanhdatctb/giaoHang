﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace giaodang.Connect.Models
{
   public class customer
    {
        int cumstomer_id;
        int user_id;
        String shopname;
        String shop_adds;
        [Key]
        public int Cumstomer_id { get => cumstomer_id; set => cumstomer_id = value; }
        public int User_id { get => user_id; set => user_id = value; }
        public string Shopname { get => shopname; set => shopname = value; }
        public string Shop_adds { get => shop_adds; set => shop_adds = value; }
    }
}
