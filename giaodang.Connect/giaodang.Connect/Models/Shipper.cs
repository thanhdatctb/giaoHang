﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace giaodang.Connect.Models
{
   public class Shipper
    {
        int shipper_id;
        String license_lates;
        int rate;
        DateTime ngaythamgia;
        int user_id;
        [Key]
        public int Shipper_id { get => shipper_id; set => shipper_id = value; }
        public string License_lates { get => license_lates; set => license_lates = value; }
        public int Rate { get => rate; set => rate = value; }
        public DateTime Ngaythamgia { get => ngaythamgia; set => ngaythamgia = value; }
        public int User_id { get => user_id; set => user_id = value; }
    }
}
