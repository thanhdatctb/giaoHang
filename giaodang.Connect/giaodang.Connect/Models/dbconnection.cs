﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using giaodang.Connect.Models;

namespace giaodang.Connect.Models
{
    public class dbconnection:DbContext
    {
        public dbconnection(DbContextOptions<dbconnection>options):base(options)
        {

        }
        public DbSet<Users> tbl_user { get; set; }

        public DbSet<Admin> tbl_admin { get; set; }
        public DbSet<Bill> tbl_bill { get; set; }
        public DbSet<customer> tbl_customer { get; set; }
        public DbSet<Feedback> tbl_feedback { get; set; }
        public DbSet<notification> tbl_notification { get; set; }
        public DbSet<product> tbl_product { get; set; }
        public DbSet<Shipper> tbl_shipper { get; set; }
        
    }
}
