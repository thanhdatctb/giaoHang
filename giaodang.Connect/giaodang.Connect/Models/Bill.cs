﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace giaodang.Connect.Models
{
   public class Bill
    {
        int bill_id;
        int shipper_id;
        int customer_id;
        String name_reciver;
        String phone_number_reciver;
        DateTime time;
        int status;
        [Key]
        public int Bill_id { get => bill_id; set => bill_id = value; }
        public int Shipper_id { get => shipper_id; set => shipper_id = value; }
        public int Customer_id { get => customer_id; set => customer_id = value; }
        [NotMapped]
        public String Name_reciver { get => name_reciver; set => name_reciver = value; }
        [NotMapped]
        public String Phone_number_reciver { get => phone_number_reciver; set => phone_number_reciver = value; }
        public DateTime Time { get => time; set => time = value; }
        public int Status { get => status; set => status = value; }
    }
}
