﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace giaodang.Connect.Models
{
  public  class notification
    {
        int notification_id;
        String notification_title;
        String notification_content;
        DateTime time;
        int admin_id;
        [Key]
        public int Notification_id { get => notification_id; set => notification_id = value; }
        public string Title { get => notification_title; set => notification_title = value; }
        public string Content { get => notification_content; set => notification_content = value; }
        public DateTime Time { get => time; set => time = value; }
        public int Admin_id { get => admin_id; set => admin_id = value; }
    }
}
