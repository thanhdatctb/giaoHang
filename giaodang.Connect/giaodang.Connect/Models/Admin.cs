﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace giaodang.Connect.Models
{
    public class Admin
    {
        int admin_id;
        String permissions;
        int user_id;
        [Key]
        public int Admin_id { get => admin_id; set => admin_id = value; }
        [Column(TypeName = "nvarchar(500)")]
        [Required]
        public string Permissions { get => permissions; set => permissions = value; }
        [Column(TypeName = "int")]
        [Required]
        public int User_id { get => user_id; set => user_id = value; }
    }
}
