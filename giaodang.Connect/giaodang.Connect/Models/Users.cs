﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace giaodang.Connect.Models
{
    public class Users
    {
        [Key]
        
        public int user_id { get; set; }
        [Column(TypeName="nvarchar(50)")]
        [Required]
        public String name { get; set; }
        [Display(Name = "Ngày Sinh")]
        [Column(TypeName = "nvarchar(50)")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [NotMapped]
       
        public String birthday { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        [Required]
        public String user_name { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        [Required]
        public String password { get; set; }
        [Column(TypeName = "bit")]
        [Required]
        public Boolean gioitinh { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public String diachi { get; set; }
        [Column(TypeName = "nchar(12)")]
        public String sdt { get; set; }
        [Column(TypeName = "nchar(100)")]
        [Required]
        public String email { get; set; }
        [Column(TypeName = "int")]
        [Required]
        public int funtion { get; set; }
        [Column(TypeName = "int")]
        [Required]
        public int status { get; set; }
    }
}
