﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace giaodang.Connect.Models
{
   public class Feedback
    {
        int feedback_id;
        String title;
        String content;
        String media;
        int cumstomer_id;
        [Key]
        public int Feedback_id { get => feedback_id; set => feedback_id = value; }
        public string Title { get => title; set => title = value; }
        public string Content { get => content; set => content = value; }
        public string Media { get => media; set => media = value; }
        public int Cumstomer_id { get => cumstomer_id; set => cumstomer_id = value; }
    }
}
