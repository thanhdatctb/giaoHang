﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace giaodang.Connect.Models
{
  public  class product
    {
        int bill_id;
        String product_name;
        String unit;
        int qty;
        float price;
        [Key]
        public int Bill_id { get => bill_id; set => bill_id = value; }
        public string Product_name { get => product_name; set => product_name = value; }
        public string Unit { get => unit; set => unit = value; }
        public int Qty { get => qty; set => qty = value; }
        public float Price { get => price; set => price = value; }
    }
}
