﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using giaodang.Connect.Models;
using Newtonsoft.Json;
using System.Text.Json;
using System.Text;

using System.Net.Http;

namespace giaodang.Connect.Controllers
{
    public class UserController : Controller
    {
        private readonly dbconnection _context;

        public UserController(dbconnection context)
        {
            _context = context;
        }

        // GET: User
        public async Task<IActionResult> Index()
        {
            return View(await _context.tbl_user.ToListAsync());
        }
      
        public async Task<IActionResult>  Login()
        {
            return  View();
        }
        [HttpPost]
        public IActionResult Logincheck( string userName,string passWord)
        {
            var str = "select * from tbl_user where user_name = " + "'" + userName + "'" + " AND password = " + "'" + passWord + "'" + " AND funtion=2";
            var result = _context.tbl_user.FromSqlRaw(str).ToList();
            if (result.Count > 0)
            {
                return RedirectToAction("Index");

            }

            return RedirectToAction("Login");
        }
        [HttpGet]
        public JsonResult Loginrequest([FromQuery]string username, [FromQuery]string password)
        {

            var str = "select * from tbl_user where user_name = " + "'" + username + "'" + " AND password = " + "'" + password + "'"+" AND user_id IS NOT NULL" ;

            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true,
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
            };
            var result = _context.tbl_user.FromSqlRaw(str).ToList();
            if(result.Count>0)
            {
                return Json(result,options);

            }
            else
                return Json("Khong tim thay user",options);
        }

        [HttpGet]
        public  JsonResult check_restiger_form_client(string name,string username,string password,string birthday,int gioitinh,string diachi,string sdt,string email,int funtion,int status)
        {
            var str = "INSERT INTO tbl_user(name,birthday,user_name,password,gioitinh,diachi,sdt,email,funtion,status) VALUES("+"'"+name+"'"+","+"'" +birthday+"'"+","+"'"+username+"'"+","+"'"+password+"'"+","+"'"+gioitinh+"'"+","+"'"+diachi+"'"+","+"'"+sdt+"'"+","+"'"+email+"'"+","+"'"+funtion+"'"+","+"'"+status+"'"+")";
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true,
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
            };

            var user = new Users();
            user.name = name;
            user.user_name = username;
            user.password = password;
            user.birthday = birthday;
            user.gioitinh = (gioitinh == 1) ? true : false;
            user.diachi = diachi;
            user.sdt = sdt;
            user.email = email;
            user.funtion = funtion;
            user.status = status;


            try
            {
            
                _context.tbl_user.Add(user);
                _context.SaveChanges();
                
                return Json("Đăng ký thành công!",options);
            }
            catch
            {

                return  Json("Đăng Ký không thành công",options);
            }
          
          
         } 
        public JsonResult  GetJson()
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true,
                Encoder= System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
        };


            return Json(_context.tbl_user.ToList(),options);


        }
        // GET: User/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.tbl_user
                .FirstOrDefaultAsync(m => m.user_id == id);
            if (users == null)
            {
                return NotFound();
            }

            return View(users);
        }

        // GET: User/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("user_id,name,birthday,user_name,password,gioitinh,diachi,sdt,email,funtion,status")] Users users)
        {
            if (ModelState.IsValid)
            {
               
                _context.Add(users);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(users);
        }

        // GET: User/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.tbl_user.FindAsync(id);
            if (users == null)
            {
                return NotFound();
            }
            return View(users);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("user_id,name,birthday,user_name,password,gioitinh,diachi,sdt,email,funtion,status")] Users users)
        {
            if (id != users.user_id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(users);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsersExists(users.user_id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(users);
        }

        // GET: User/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
          
            var users = await _context.tbl_user
                .FirstOrDefaultAsync(m => m.user_id == id);
            if (users == null)
            {
                return NotFound();
            }

            return View(users);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //List<Reservation> reservationList = new List<Reservation>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.DeleteAsync("https://localhost:44361/api/tbl_user/"+id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    //reservationList = JsonConvert.DeserializeObject<List<Reservation>>(apiResponse);
                }
            }
            return RedirectToAction(nameof(Index));
            var admin = await _context.tbl_admin.FirstOrDefaultAsync(m => m.User_id == id);
            if (admin != null)
            {
                var notifications = _context.tbl_notification.Select(r => r.Admin_id);
                var list_notification = _context.tbl_notification.Where(r => notifications.Contains(admin.Admin_id));
                foreach (var item in list_notification)
                {
                    _context.tbl_notification.Remove(item);
                }
                _context.tbl_admin.Remove(admin);
            }
            var customer = await _context.tbl_customer.FirstOrDefaultAsync(m => m.User_id == id);
            if (customer != null)
            {
                var bills = _context.tbl_bill.Select(r => r.Customer_id);
                var list_bills = _context.tbl_bill.Where(r => bills.Contains(customer.Cumstomer_id));
                foreach (var item in list_bills)
                {
                    var product = _context.tbl_product.Select(r => r.Bill_id);
                    var list_product = _context.tbl_product.Where(r => product.Contains(item.Bill_id));
                    foreach (var item_product in list_product)
                    {
                        _context.tbl_product.Remove(item_product);
                    }
                    _context.tbl_bill.Remove(item);
                }
                var feedback = _context.tbl_feedback.Select(r => r.Cumstomer_id);
                var list_feedback = _context.tbl_feedback.Where(r => feedback.Contains(customer.Cumstomer_id));
                foreach (var item in list_feedback)
                {
                    _context.tbl_feedback.Remove(item);
                }
                _context.tbl_customer.Remove(customer);
            }
            var shipper = await _context.tbl_shipper.FirstOrDefaultAsync(m => m.User_id == id);

            if (shipper != null)
            {
                var bills = _context.tbl_bill.Select(r => r.Shipper_id);
                var list_bills = _context.tbl_bill.Where(r => bills.Contains(shipper.Shipper_id));
                foreach (var item in list_bills)
                {
                    var product = _context.tbl_product.Select(r => r.Bill_id);
                    var list_product = _context.tbl_product.Where(r => product.Contains(item.Bill_id));
                    foreach (var item_product in list_product)
                    {
                        _context.tbl_product.Remove(item_product);
                    }
                    _context.tbl_bill.Remove(item);
                }

                _context.tbl_shipper.Remove(shipper);
            }
            var users = await _context.tbl_user.FindAsync(id);
            _context.tbl_user.Remove(users);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UsersExists(int id)
        {
            return _context.tbl_user.Any(e => e.user_id == id);
        }
    }
}
