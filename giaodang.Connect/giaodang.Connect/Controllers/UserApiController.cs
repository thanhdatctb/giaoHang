﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using giaodang.Connect.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace giaodang.Connect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserApiController : ControllerBase
    {
        private readonly dbconnection _context ;
        public UserApiController(dbconnection context)
        {
            _context = context;
        }
        // GET: api/<UserApiController>
        [HttpGet]
        public IEnumerable<Users> Get()
        {
            return _context.tbl_user.ToList();
            //return new string[] { "value1", "value2" };
        }

        // GET api/<UserApiController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<UserApiController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<UserApiController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UserApiController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
