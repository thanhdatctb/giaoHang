﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using giaodang.Connect.Models;
using System.Text.Json;

namespace giaodang.Connect.Controllers
{
    public class notificationsController : Controller
    {
        private readonly dbconnection _context;

        public notificationsController(dbconnection context)
        {
            _context = context;
        }

        // GET: notifications
        public async Task<IActionResult> Index()
        {
            return View(await _context.tbl_notification.ToListAsync());
        }

        // GET: notifications/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.tbl_notification
                .FirstOrDefaultAsync(m => m.Notification_id == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }
        public JsonResult GetJson()
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true,
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
            };


            return Json(_context.tbl_notification.ToList(), options);


        }
        // GET: notifications/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: notifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Notification_id,Title,Content,Time,Admin_id")] notification notification)
        {
            if (ModelState.IsValid)
            {
                _context.Add(notification);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(notification);
        }

        // GET: notifications/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.tbl_notification.FindAsync(id);
            if (notification == null)
            {
                return NotFound();
            }
            return View(notification);
        }

        // POST: notifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Notification_id,Title,Content,Time,Admin_id")] notification notification)
        {
            if (id != notification.Notification_id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(notification);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!notificationExists(notification.Notification_id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(notification);
        }

        // GET: notifications/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.tbl_notification
                .FirstOrDefaultAsync(m => m.Notification_id == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // POST: notifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notification = await _context.tbl_notification.FindAsync(id);
            _context.tbl_notification.Remove(notification);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool notificationExists(int id)
        {
            return _context.tbl_notification.Any(e => e.Notification_id == id);
        }
    }
}
